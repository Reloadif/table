#pragma once

#include <iostream>
#include <iomanip>
// #include <string>
// #include <exception>

#include "AbstractTable.h"
#include "TableRecord.h"

template <typename typeValue>
class ViewableTable : public AbstractTable<typeValue, TableRecord>
{
	template <typename typeValue> friend std::ostream& operator << (std::ostream& out, ViewableTable<typeValue>& viewableTable);

public:
	ViewableTable() = delete;

	ViewableTable(const int _sizeOfTable);
	ViewableTable(const ViewableTable& viewableTable);

	int findRecord(const std::string& _key) override;
	void insertRecord(const std::string& _key, typeValue _value) override;
	void deleteRecord(const std::string& _key) override;
};


template <typename typeValue>
ViewableTable<typeValue>::ViewableTable(const int _sizeOfTable) : AbstractTable<typeValue, TableRecord>(_sizeOfTable) {

}
template <typename typeValue>
ViewableTable<typeValue>::ViewableTable(const ViewableTable& viewableTable) : AbstractTable<typeValue, TableRecord>(viewableTable) {

}


template <typename typeValue>
int ViewableTable<typeValue>::findRecord(const std::string& _key) {
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	for (int record = 0; record < this->numberOfRecords; ++record) {
		TableRecord<typeValue> recording = recordArray[record];
		this->efficiencyIndicator++;
		if (_key == recording.getKey()) {
			return record;
		}
	}
	return -1;
}
template <typename typeValue>
void ViewableTable<typeValue>::insertRecord(const std::string& _key, typeValue _value) {
	int idRecord = findRecord(_key);
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	if (idRecord == -1) {
		recordArray[this->numberOfRecords] = TableRecord<typeValue>(_key, _value);
		this->numberOfRecords++;
	}
	else {
		TableRecord<typeValue>& recording = recordArray[idRecord];
		recording.setValue(recording.getValue() + _value);
	}
}
template <typename typeValue>
void ViewableTable<typeValue>::deleteRecord(const std::string& _key) {
	int idRecord = findRecord(_key);
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	if (idRecord != -1) {
		if (idRecord != this->numberOfRecords-1) {
			recordArray[idRecord] = recordArray[this->numberOfRecords - 1];
		}
		this->numberOfRecords--;
	}
}


template <typename typeValue>
std::ostream& operator << (std::ostream& out, ViewableTable<typeValue>& viewableTable) {
	TableRecord<typeValue>* recordArray = viewableTable.getArrayOfRecords();

	std::cout << "ID |"
		<< std::setw(10) << "Key |"
		<< std::setw(12) << "Value |"
		<< "\t"
		<< std::endl;

	for (int record = 0; record < viewableTable.getNumberOfRecords(); ++record) {
		TableRecord<typeValue> recordingAtStep = recordArray[record];
		
		std::cout << record + 1 
			<< std::setw(10) << recordingAtStep.getKey()
		    << std::setw(12) << recordingAtStep.getValue()
			<< "\t"
			<< std::endl;
	}

	return out;
}
