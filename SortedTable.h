#pragma once

// #include <iostream>
// #include <iomanip>
// #include <string>
// #include <exception>

// #include "AbstractTable.h"
// #include "TableRecord.h"
#include "ViewableTable.h"
#include "Sorting.h"

template <typename typeValue>
class SortedTable : public AbstractTable<typeValue, TableRecord>
{
	template <typename typeValue> friend std::ostream& operator << (std::ostream& out, SortedTable<typeValue>& viewableTable);

public:
	SortedTable() = delete;

	SortedTable(const int _sizeOfTable);
	SortedTable(const SortedTable& sortedTable);

	SortedTable(ViewableTable<typeValue>& viewableTable);

	int findRecord(const std::string& _key) override;
	void insertRecord(const std::string& _key, typeValue _value) override;
	void deleteRecord(const std::string& _key) override;

};


template <typename typeValue>
SortedTable<typeValue>::SortedTable(const int _sizeOfTable) : AbstractTable<typeValue, TableRecord>(_sizeOfTable) {

}
template <typename typeValue>
SortedTable<typeValue>::SortedTable(const SortedTable& sortedTable) : AbstractTable<typeValue, TableRecord>(sortedTable) {

}


template <typename typeValue>
SortedTable<typeValue>::SortedTable(ViewableTable<typeValue>& viewableTable) : AbstractTable<typeValue, TableRecord>(viewableTable.getSizeOfTable()) {
	TableRecord<typeValue>* viewableArray = viewableTable.getArrayOfRecords();
	TableRecord<typeValue>* sortedArray = this->getArrayOfRecords();

	for (int i = 0; i < viewableTable.getNumberOfRecords(); i++) {
		sortedArray[i] = viewableArray[i];
	}

	this->numberOfRecords = viewableTable.getNumberOfRecords();

	modifiedQuickSort<typeValue>(sortedArray, this->numberOfRecords);
}


template <typename typeValue>
int SortedTable<typeValue>::findRecord(const std::string& _key) {

	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();
	int first = 0, last = this->numberOfRecords - 1;

	while (last > first) {
		this->efficiencyIndicator++;
		int middle = (first + last) / 2; 

		if (recordArray[middle] < _key) {
			first = middle + 1;
		}
		else if (recordArray[middle] > _key) {
			last = middle - 1;
		}
		else {
			return middle;
		}
	}

	if (recordArray[last] == _key) {
		return last;
	}

	return -1;
}
template <typename typeValue>
void SortedTable<typeValue>::insertRecord(const std::string& _key, typeValue _value) {
	int idRecord = findRecord(_key);
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	if (idRecord == -1) {
		int index;

		for (index = 0; index < this->numberOfRecords; index++) {
			this->efficiencyIndicator++;
			TableRecord<typeValue> recordAtStep = recordArray[index];
			if (_key < recordAtStep.getKey()) {
				break;
			}
			else if (index == this->numberOfRecords - 1) {
				index = this->numberOfRecords;
				break;
			}
		}

		for (int i = this->numberOfRecords; i > index; i--) {
			this->efficiencyIndicator++;
			recordArray[i] = recordArray[i - 1];
		}

		recordArray[index] = TableRecord<typeValue>(_key, _value);
		this->numberOfRecords++;
	}
	else {
		TableRecord<typeValue>& recording = recordArray[idRecord];
		recording.setValue(recording.getValue() + _value);
	}
}
template <typename typeValue>
void SortedTable<typeValue>::deleteRecord(const std::string& _key) {
	int idRecord = findRecord(_key);
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();
	
	if (idRecord != -1) {
		while (idRecord != this->numberOfRecords - 1) {
			this->efficiencyIndicator++;
			recordArray[idRecord] = recordArray[idRecord + 1];
			++idRecord;
		}
		this->numberOfRecords--;
	}
}


template <typename typeValue>
std::ostream& operator << (std::ostream& out, SortedTable<typeValue>& sortedTable) {
	TableRecord<typeValue>* recordArray = sortedTable.getArrayOfRecords();

	std::cout << "ID |"
		<< std::setw(10) << "Key |"
		<< std::setw(12) << "Value |"
		<< "\t"
		<< std::endl;

	for (int record = 0; record < sortedTable.getNumberOfRecords(); ++record) {
		TableRecord<typeValue> recordingAtStep = recordArray[record];

		std::cout << record + 1
			<< std::setw(10) << recordingAtStep.getKey()
			<< std::setw(12) << recordingAtStep.getValue()
			<< "\t"
			<< std::endl;
	}

	return out;
}