#include "Hash.h"

Hash::Hash() {

}


int Hash::receivingExistCode(int code) {
	code += 256;

	while ( !((code >= 48 && code <= 57) || (code >= 65 && code <= 90) || (code >= 97 && code <= 122)) ) {
		if (code < 48) code += 24;
		else code -= 47;
	}

	return code;
}

int Hash::getControlSum(std::string _string) {
	unsigned int salt = 0;

	for (int i = 0; i < _string.length(); i++) {
		salt += (int)_string[i];
	}

	return salt;
}

Hash& Hash::getInstance() {
	static Hash instance;
	return instance;
}

std::string Hash::getHashString(std::string userString) {

	unsigned int minLen = 16;

	unsigned int realMinLen = 16;

	unsigned int userStringSalt = getControlSum(userString);
	unsigned int userStringLength = (userString.length());

	while (minLen < userStringLength) {
		minLen *= 2;
	}

	if ((minLen - userStringLength) < minLen) {
		minLen *= 2;
	}

	int addCount = minLen - userStringLength;

	for (int i = 0; i < addCount; i++) {
		userString += receivingExistCode(userString[i] + userString[i + 1]);
	}

	unsigned int maxStringSalt = getControlSum(userString);
	unsigned int maxStringLength = (userString.length());

	while (userString.length() != realMinLen) {

		for (int i = 0, center = userString.length() / 2; i < center; i++) {
			hashString += receivingExistCode(userString[center - i] + userString[center + i]);
		}

		userString = hashString;
		hashString.clear();
	}

	return userString;
}
