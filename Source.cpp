// #include <string>
// #include <exception>
// #include <iostream>
// #include <iomanip>
// #include <vld.h> �������� �� ������ ������

#include "SortedTable.h"
#include "ViewableTable.h"
#include "TreeTable.h"
#include "HashTable.h"

void showMainMenu(int& choice, int& sizeOfTable) {
	do {
		std::cout << "��� ������ :" << std::endl;
		std::cout << "1. ��������������� �������" << std::endl;
		std::cout << "2. ����������� �������" << std::endl;
		std::cout << "3. ��� �������" << std::endl;
		std::cout << "4. �������� �������" << std::endl << std::endl;

		std::cin >> choice;
	} while (choice <= 0 || choice > 4);

	do {
		std::cout << "������ ������� : ";
		std::cin >> sizeOfTable;
	} while (sizeOfTable <= 0);
}
void showOperationMenu(int& choice) {
	do {
		system("cls");
		std::cout << "�������� ��� �������� :" << std::endl;
		std::cout << "1. �������� ������ � �������" << std::endl;
		std::cout << "2. ������� ������ �� �������" << std::endl;
		std::cout << "3. ����� ������ � �������" << std::endl;
		std::cout << "4. ������� ��� �������" << std::endl;
		std::cout << "5. ������� ������������� ��������" << std::endl << std::endl;
		std::cout << "   < 0 - ����� �� ��������� >" << std::endl << std::endl;

		std::cin >> choice;
	} while (choice < 0 || choice > 5);
}

int main() {
	using namespace std;
	setlocale(LC_ALL, "Russian");

	int choice = 0, sizeOfTable = 0;
	showMainMenu(choice, sizeOfTable);

	ViewableTable<int> viewableTable(sizeOfTable);
	SortedTable<int> sortedTable(sizeOfTable);
	HashTable<int> hashTable(sizeOfTable);
	TreeTable<int> treeTable;

	AbstractTable<int, TableRecord>* tableArray[3] = {&viewableTable, &sortedTable, &hashTable};

	int operation = 1;
	while (operation != 0) {
		
		showOperationMenu(operation);

		if (choice != 4) {
			string key = "";
			int value = 0;
			int index = -1;

			switch (operation) {
			case 1:
				cout << "������� ���� : ";
				cin >> key;
				cout << "������� �������� : ";
				cin >> value;

				if(!tableArray[choice - 1]->isFull())
					tableArray[choice-1]->insertRecord(key, value);
				break;
			case 2:
				cout << "������� ���� : ";
				cin >> key;

				if(!tableArray[choice - 1]->isEmpty())
					tableArray[choice-1]->deleteRecord(key);
				break;
			case 3:
				cout << "������� ���� : ";
				cin >> key;

				index = tableArray[choice - 1]->findRecord(key);

				std::cout << "ID |"
					<< std::setw(10) << "Key |"
					<< std::setw(12) << "Value |"
					<< "\t"
					<< std::endl;
				if (index != -1) {
					std::cout << index
						<< std::setw(10) << (tableArray[choice - 1]->getArrayOfRecords())[index].getKey()
						<< std::setw(12) << (tableArray[choice - 1]->getArrayOfRecords())[index].getValue()
						<< "\t"
						<< std::endl;
				}
					
				break;
			case 4:
				if (choice == 1) {
					ViewableTable<int>* outputTable = dynamic_cast<ViewableTable<int>*>(tableArray[choice-1]);
					cout << *outputTable;
				}
				else if (choice == 2) {
					SortedTable<int>* outputTable = dynamic_cast<SortedTable<int>*>(tableArray[choice-1]);
					cout << *outputTable;
				}
				else if(choice == 3) {
					HashTable<int>* outputTable = dynamic_cast<HashTable<int>*>(tableArray[choice-1]);
					cout << *outputTable;
				}
				break;
			case 5:
				cout << "���������� �������� : " << tableArray[choice - 1]->getEfficiencyIndicator();
				break;
			}
		}
		else {
			string key = "";
			int value = 0;
			TreeRecord<int>* record = nullptr;

			switch (operation) {
			case 1:
				cout << "������� ���� : ";
				cin >> key;
				cout << "������� �������� : ";
				cin >> value;
				treeTable.insertRecord(key, value);
				break;
			case 2:
				cout << "������� ���� : ";
				cin >> key;
				treeTable.deleteRecord(key);
				break;
			case 3:
				cout << "������� ���� : ";
				cin >> key;
				record = treeTable.findRecord(key);

				std::cout << "ID |"
					<< std::setw(10) << "Key |"
					<< std::setw(12) << "Value |"
					<< "\t"
					<< std::endl;
				if (record != nullptr) {
					std::cout << 1
						<< std::setw(10) << record->getKey()
						<< std::setw(12) << record->getValue()
						<< "\t"
						<< std::endl;
				}

				break;
			case 4:
				cout << treeTable;
				break;
			case 5:
				cout << "���������� �������� : " << treeTable.getEfficiencyIndicator();
				break;
			}
		}

		cout << endl << endl;
		system("pause");
	}


	return 0;
}