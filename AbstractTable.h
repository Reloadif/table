#pragma once

// #include <string>
// #include <exception>

#include "TableRecordArray.h"

template <typename typeValue, template<typename typeV> class typeRecord >
class AbstractTable {
protected:
	int numberOfRecords;
	int efficiencyIndicator;

	TableRecordArray<typeValue, typeRecord> tableRecordArray;

	AbstractTable(const int _sizeOfTable = 10);
	AbstractTable(const AbstractTable& abstractTable);

	AbstractTable& operator = (const AbstractTable& abstractTable);

public:
	typeRecord<typeValue>* getArrayOfRecords();

	int getNumberOfRecords() const;
	int getEfficiencyIndicator() const;
	int getSizeOfTable() const;

	bool isEmpty() const;
	bool isFull() const;

	virtual int findRecord(const std::string& _key) = 0;
	virtual void insertRecord(const std::string& _key, typeValue _value) = 0;
	virtual void deleteRecord(const std::string& _key) = 0;
};


template <typename typeValue, template<typename typeV> class typeRecord >
AbstractTable<typeValue, typeRecord>::AbstractTable(const int _sizeOfTable) 
: tableRecordArray(_sizeOfTable), numberOfRecords(0), efficiencyIndicator(0) {

}
template <typename typeValue, template<typename typeV> class typeRecord >
AbstractTable<typeValue, typeRecord>::AbstractTable(const AbstractTable& abstractTable)
: tableRecordArray(abstractTable.tableRecordArray), numberOfRecords(abstractTable.numberOfRecords), efficiencyIndicator(abstractTable.efficiencyIndicator) {

}


template <typename typeValue, template<typename typeV> class typeRecord >
AbstractTable<typeValue, typeRecord>& AbstractTable<typeValue, typeRecord>::operator = (const AbstractTable& abstractTable) {

	if (this == &abstractTable) {
		return *this;
	}

	numberOfRecords = abstractTable.numberOfRecords;
	efficiencyIndicator = abstractTable.efficiencyIndicator;

	tableRecordArray = abstractTable.tableRecordArray;

	return *this;
}

template <typename typeValue, template<typename typeV> class typeRecord >
typeRecord<typeValue>* AbstractTable<typeValue, typeRecord>::getArrayOfRecords() {
	return tableRecordArray.getArrayOfRecords();
}

template <typename typeValue, template<typename typeV> class typeRecord >
int AbstractTable<typeValue, typeRecord>::getNumberOfRecords() const {
	return numberOfRecords;
}
template <typename typeValue, template<typename typeV> class typeRecord >
int AbstractTable<typeValue, typeRecord>::getEfficiencyIndicator() const {
	return efficiencyIndicator;
}
template <typename typeValue, template<typename typeV> class typeRecord >
int AbstractTable<typeValue, typeRecord>::getSizeOfTable() const {
	return tableRecordArray.sizeOfTable;
}


template <typename typeValue, template<typename typeV> class typeRecord >
bool AbstractTable<typeValue, typeRecord>::isEmpty() const {
	return numberOfRecords == 0;
}
template <typename typeValue, template<typename typeV> class typeRecord >
bool AbstractTable<typeValue, typeRecord>::isFull() const {
	return numberOfRecords == tableRecordArray.sizeOfTable;
}