#pragma once

#include "TableRecord.h"


template <typename typeValue>
void insertSort(TableRecord<typeValue>* arrayData, const int sizeData);

template <typename typeValue>
void quickSort(TableRecord<typeValue>* arrayData, const int sizeData);

template <typename typeValue>
void modifiedQuickSort(TableRecord<typeValue>* arrayData, const int sizeData);




template <typename typeValue>
void swap(TableRecord<typeValue>& leftValue, TableRecord<typeValue>& righValue) {
	TableRecord<typeValue> buffer = righValue;
	righValue = leftValue;
	leftValue = buffer;
}




//-- ���������� ��������� --//

template <typename typeValue>
void insertSort(TableRecord<typeValue>* arrayData, const int sizeData) {

	for (int i = 1; i < sizeData; ++i) {
		for (int j = 0; j < i; ++j) {

			if (arrayData[j] > arrayData[i]) swap<typeValue>(arrayData[i], arrayData[j]);

		}
	}

}




//-- ������� ���������� --//

const int validForInsert = 30;

template <typename typeValue>
void quickSort(TableRecord<typeValue>* arrayData, const int sizeData) {

	int first = 0, last = sizeData - 1;
	TableRecord<typeValue> medianValue = arrayData[sizeData / 2];

	do {
		while (arrayData[first] < medianValue) first++;
		while (arrayData[last] > medianValue) last--;

		if (first <= last) {
			swap<typeValue>(arrayData[first], arrayData[last]);
			first++; last--;
		}
	} while (first <= last);

	if (last > validForInsert) quickSort<typeValue>(arrayData, last);
	if (sizeData > first + validForInsert) quickSort<typeValue>( (arrayData + first) , sizeData - first);

}



//-- ������������������� ������� ���������� --//

template <typename typeValue>
void modifiedQuickSort(TableRecord<typeValue>* arrayData, const int sizeData) {
	if(sizeData > validForInsert) {
		quickSort<typeValue>(arrayData, sizeData-1);
	}
	insertSort<typeValue>(arrayData, sizeData);
}